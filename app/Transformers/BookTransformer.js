'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')
const UserTransformer = use('App/Transformers/UserTransformer')
/**
 * BookTransformer class
 *
 * @class BookTransformer
 * @constructor
 */
class BookTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  static get availableInclude() {
    return [
      'user'
    ]
  }

  transform(book) {
    return {
      id: book.id,
      title: book.title,
      isbn: book.isbn,
      publisher: book.publisher_name,
      author: book.author_name,
      // created: book.created_at,
      // updated: book.updated_at
    }
  }

  includeUser(book) {
    return this.item(book.getRelated('user'), UserTransformer)
  }
}

module.exports = BookTransformer
