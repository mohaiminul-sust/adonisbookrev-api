'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class UserTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  static get availableInclude() {
    return [
      'books'
    ]
  }

  transform (user) {
    return {
      id: user.id,
      username: user.username,
      email: user.email,
      // created: user.created_at,
      // updated: user.updated_at
    }
  }

  includeBooks(user) {
    return this.collection(user.getRelated('books'), BookTransformer)
  }
}

module.exports = UserTransformer
