'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/framework/src/Auth')} Auth */

class TokenDetector {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({response, auth}, next) {
    
    try {
      await auth.check()
    } catch (error) {
      return response.json({
        status: 403,
        response: {
          message: 'Not Authorized! JWT authentication failed!'
        }
      })
    }

    await next()
  }
}

module.exports = TokenDetector
