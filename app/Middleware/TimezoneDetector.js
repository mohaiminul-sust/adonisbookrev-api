'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

var geoip = use('geoip-lite')

class TimezoneDetector {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request }, next) {
    const ip = request.ip()
    const geo = geoip.lookup(ip)

    if(geo) {
      request.country = geoip.lookup(ip).country
      request.timezone = geoip.lookup(ip).timezone
    }

    await next()
  }
}

module.exports = TimezoneDetector
