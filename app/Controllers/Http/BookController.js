'use strict'
const Book = use('App/Models/Book')
const Bumblebee = use('Adonis/Addons/Bumblebee')
const perPage = 10

class BookController {
    async index ({request, response}) {
        const include = request.get().include
        const page = request.get().page

        var books = {}
        if(page) {
           books = await Book.query().paginate(page, perPage)
        } else {
           books = await Book.all()
        }

        if(page && include) {
            return Bumblebee.create().include(include).paginate(books, 'BookTransformer')
        } else if (page) {
            return Bumblebee.create().paginate(books, 'BookTransformer')
        } else if (include) {
            return Bumblebee.create().include(include).collection(books, 'BookTransformer')
        }

        return Bumblebee.create().collection(books, 'BookTransformer')
    }

    async show ({params, request, response}) {
        const include = request.get().include
        
        const book = await Book.find(params.id)
        if(!book) {
            return response.status(404).json({data: 'Book not found'})            
        }

        if(include) {
            return Bumblebee.create().include(include).item(book, 'BookTransformer')
        }
        return Bumblebee.create().item(book, 'BookTransformer')
    }

    async store ({request, auth, response}) {
        const bookInfo = request.only(['title', 'isbn', 'publisher_name', 'author_name'])

        const book = new Book()
        book.title = bookInfo.title
        book.isbn = bookInfo.isbn
        book.publisher_name = bookInfo.publisher_name
        book.author_name = bookInfo.author_name
        book.user_id = auth.user.id
        await book.save()

        return Bumblebee.create().item(book, 'BookTransformer')
    }

    async update ({params, request, response}) {
        const bookInfo = request.only(['title', 'isbn', 'publisher_name', 'author_name'])

        const book = await Book.find(params.id)
        if (!book) {
            return response.status(404).json({data: 'Book not found'})
        }
        book.title = bookInfo.title
        book.isbn = bookInfo.isbn
        book.publisher_name = bookInfo.publisher_name
        book.author_name = bookInfo.author_name
        await book.save()

        return Bumblebee.create().item(book, 'BookTransformer')
    }

    async delete ({params, response}) {
        const book = await Book.find(params.id)
        if (!book) {
            return response.status(404).json({data: 'Book not found'})
        }

        await book.delete()
        return response.status(200).json({data: 'Book Deleted!'})
    }
}

module.exports = BookController
