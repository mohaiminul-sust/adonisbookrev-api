'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/User', (faker) => {
    return {
        username: 'Andromeda',//faker.username(),
        password: 'admin',
        email: 'mohaiminul.sust@gmail.com'//faker.email()
    }
})

Factory.blueprint('App/Models/Book', (faker) => {
    return {
        title: faker.word({ syllables: 3 }) + ' ' + faker.word({ syllables: 2 }),
        isbn: faker.guid(),
        publisher_name: faker.name(),
        author_name: faker.name()
    }
})