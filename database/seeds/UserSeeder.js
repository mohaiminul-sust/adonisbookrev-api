'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class UserSeeder {
  async run () {
    const user = await Factory
      .model('App/Models/User')
      .create()
    console.log('Seeded User : ', user)

    const books = await Factory
      .model('App/Models/Book')
      .makeMany(56)
    console.log('Seeded Books : ' + books.length)
    // console.log(books)

    await user.books().saveMany(books)
    console.log('Assigned Books to User...')
  }
}

module.exports = UserSeeder
