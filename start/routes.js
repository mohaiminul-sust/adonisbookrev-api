'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Welcome to BooksReview API!' }
})

Route.group(()=> {
  Route.post('register', 'AuthController.register')
  Route.post('login', 'AuthController.login')
}).prefix('api/v1/auth');

Route.group(() => {
  Route.post('books', 'BookController.store')
  Route.get('books', 'BookController.index')
  Route.get('books/:id', 'BookController.show')
  Route.put('books/:id', 'BookController.update')
  Route.delete('books/:id', 'BookController.delete')
}).prefix('api/v1').middleware(['api']);
